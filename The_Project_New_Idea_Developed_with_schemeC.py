#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import The_project_Week2_Newtonmethod_modified as NM


# In[23]:


#the new method Girard and Delage (1990) provided
def SchemeJPlus(X0,n,P,dt,K,delta):#delta is described in the word doc I sent to you
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)

    t[0]=0
    X[0]=(1/K)**(1/(P+1))
    ttest=[]
    Xtest=[]
    j=0
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        beta=np.max([1/2,delta-1/(K*X[i-1]**P*dt)/(P+1)])

#        beta=1
#        beta=1/2
        Xnp1=(X[i-1]-K*X[i-1]**(P+1)*(1-beta*(P+1))+S)/(1+beta*(P+1)*K*X[i-1]**P)
        X0=(S/K)**(1/(P+1))

        X[i]=Xnp1
        t[i]=dt*i
        if beta==1/2:
            Xnp1=2*NM.iterativeB(0,P+1,K*dt/2,S*dt/2+X[i-1])-X[i-1]#SchemeC
            ttest.append(t[i])
            Xtest.append(Xnp1)

        X[i]=Xnp1
        t[i]=dt*i
    return X,t,Xtest,ttest


# In[24]:


P=2
dt=21
A=SchemeJPlus(0,1000,P,dt,10,1)[1]
B=SchemeJPlus(0,1000,P,dt,10,1)[0]
plt.plot(A,B)
plt.xlim(0,10000)
plt.show()


# In[17]:


#RK4 method
def f(X,t,P,S,K):#f is the function of local rate of change of ground temperature
    f=-(K*X**P)*X+S
    return f
def SchemeRK4(X0,n,P,dt,K): #RK4 method, X0 is the initial value of ground temperature
    n=n+1
    X=np.zeros(n)           #n is total number of time step
    t=np.zeros(n)           #P represent the nonlinear relation of exchange coefficient with temperature
    t[0]=0                  #dt, time step
    S=1+np.sin(2*np.pi*(0)/20*dt)
    X0=(S/K)**(1/(P+1))
    X[0]=X0                 

    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        k1=f(X[i-1],i*dt,P,S,K)
        k2=f(X[i-1]+dt*k1/2,i*dt+dt/2,P,S,K)
        k3=f(X[i-1]+dt*k2/2,i*dt+dt/2,P,S,K)
        k4=f(X[i-1]+dt*k3,i*dt+dt,P,S,K)
        X[i]=X[i-1]+dt*(1/6)*(k1+2*k2+2*k3+k4)
        t[i]=dt*i
    return X,t


# In[18]:


n=1#n is any non-negative integer including 0
dt=1+20*n
A=SchemeJPlus(0,100,4,dt,10,0.8)[0]
B=SchemeJPlus(0,100,4,dt,10,0.8)[1]
plt.plot(B,A)


# In[ ]:





# In[19]:


def SchemeH(X0,n,P,dt,K,A):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1)) 
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xstar=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)#X*
        Xnp1=(1-A)*Xstar+A*X[i-1]
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-A)*((1-alpha*P)/(1+alpha))+A#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[ ]:





# In[25]:


dt=0.1
P=2
plt.figure(figsize=(25,10))

A=SchemeJPlus(0,1000,P,dt,10,1)[0]
B=SchemeJPlus(0,1000,P,dt,10,1)[1]
C=SchemeRK4(0,30000,P,0.1,10)[0]
D=SchemeRK4(0,30000,P,0.1,10)[1]
CX=NM.SchemeC(0,1000,P,dt,10)[1]
CY=NM.SchemeC(0,1000,P,dt,10)[0]
TestJt=SchemeJPlus(0,1000,P,dt,10,1)[3]
TestJX=SchemeJPlus(0,1000,P,dt,10,1)[2]
#plt.scatter(B,A,s=100,label='Scheme JPlus',marker='X')
plt.plot(B,A,label='Scheme JPlus')
plt.scatter(TestJt,TestJX,label='Scheme JPlus when beta=1/2')
plt.plot(CX,CY,label='schemec')
plt.plot(D,C,c='red',label='Exact')
plt.plot()
plt.legend()

plt.xlim(0,100)


# In[21]:


def errofJplus(X0,P,K,delta,Maxdt):
    dtseries=np.arange(1,Maxdt,1)
    err=np.zeros(len(dtseries))
    n=100
    index=0
    TVD=np.zeros(len(dtseries))
    
    for dt in dtseries:
        d=int(dt/0.1)
        JPlusValues = SchemeJPlus(X0,n,P,dt,K,delta)[0]
        ExactValues = SchemeRK4(X0,n*d,P,0.1,K)[0]
        FiltExactValues=np.zeros(len(JPlusValues))
        
        for i in range(len(JPlusValues)):
            FiltExactValues[i]=ExactValues[i*d]
        VDExact=[np.abs(FiltExactValues[i+1]-FiltExactValues[i]) for i in range(len(JPlusValues)-1)]
        VDJPlus=[np.abs(JPlusValues[i+1]-JPlusValues[i]) for i in range(len(JPlusValues)-1)]
        errs=np.abs(np.array(FiltExactValues)-np.array(JPlusValues))
        err[index]=errs.std()
        index+=1
    plt.plot(dtseries,err)
    plt.ylabel('error')


# In[22]:


dt=1
P=2
errofJplus(0,P,10,1,80)


# In[ ]:





# In[ ]:




