#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# In[2]:


def iterativeB(X,P,A,B): #the iterative method only suitable in form A*X**P+X-B=0, which X is the unknowner power, A,B,P are the coefficents.
    d=2
    Y=A*X**P+X-B
    while (d>=1e-6):
        Yd=P*A*X**(P-1)+1
        if Yd==0:
            if Y==0:
                break
            else:
                while Yd==0:
                    X+=1
                    Yd=P*A*X**(P-1)+1
        X=-Y/Yd+X
        Y=A*X**P+X-B
        d=np.abs(Y-0)
    return X


# In[3]:


#Backward implicit(scheme.b)
#We know the solution of the X(n+1) is like A*X**3+X=B, which A=K*dt, B=S*dt+X(n) which can be represent as constant in certain n,
def SchemeB(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))   #when i=0 ,S=1
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)# I think something unclearly explained with the Formula of S in the article,
                                    # only in this form can it output similar graph with different dt, on the articl wrote S=1+np.sin(2*np.pi*i/20)
        Xe=(S/K)**(1/(P+1))#equilibrium solution
        X[i]=iterativeB(0,P+1,K*dt,S*dt+X[i-1])
        t[i]=dt*i
        
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=1/(1+alpha*(P+1))#amplification factor
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[4]:


#Crank-Nicholson, Scheme.c, there is a problem, if dt is little big, such as 1, the initial point will cause a tremble.

#We know the solution of the (X(n+1)+X(n))/2 is like A*X**3+X=B(the same as scheme.b),
#which A=K*dt/2, B=S*dt/2+X[n] which can be represented as constant in certain n.
def SchemeC(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))
    TVD=0
    for i in range(1,n):
        S=1+np.sin(2*np.pi/20*(i)*dt)
        Xnp1=2*iterativeB(0,P+1,K*dt/2,S*dt/2+X[i-1])-X[i-1]#(X(n+1)
        Xe=(S/K)**(1/(P+1))#equilibrium solution
        X[i]=Xnp1
        t[i]=dt*i
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-alpha*(P+1)/2)/(1+alpha*(P+1)/2)#amplification factor
        TVD+=np.abs(X[i]-X[i-1])
        if abs(X[i]) > 1000:
            break
        
    return X,t,TVD


# In[5]:


#Explicit coefficent implicit temperature, Scheme.d

def SchemeD(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))  
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xnp1=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-alpha*(P))/(1+alpha)#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
    
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[6]:


#Predictor-corrector coefficient, implicit temperature, scheme.e
def SchemeE(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xstar=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)#X*
        Xnp1=(X[i-1]+S*dt)/(1+K*Xstar**P*dt)
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-alpha*(P-1)+(alpha*P)**2)/(1+alpha)**2#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[7]:


#Averaged coefficient (t*,tn), implicit temperature,scheme.f
def SchemeF(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))  
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xstar=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)#X*
        Xnp1=(X[i-1]+S*dt)/(1+K*(X[i-1]**P+Xstar**P)/2*dt)
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-alpha*(P-1)*(alpha**2*P/2)+(alpha*P)**2/2)/(1+alpha)**2#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[8]:


#Explicit coefficient, extrapolated temperature, scheme.g

def SchemeG(X0,n,P,dt,K,gamma):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1)) 
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xnp1=(-K*X[i-1]**3*(1-gamma)*dt+S*dt+X[i-1])/(1+K*gamma*dt*X[i-1]**P)
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-alpha*(P+1-gamma))/(1+alpha*gamma)#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[9]:


#Explicit coefficient, implcit temperature,followed by time filter scheme.h

def SchemeH(X0,n,P,dt,K,A):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1)) 
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xstar=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)#X*
        Xnp1=(1-A)*Xstar+A*X[i-1]
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-A)*((1-alpha*P)/(1+alpha))+A#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[10]:


#Double time step, explicit coefficient, implicit temperature, followed by time average, scheme.i
def SchemeI(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1)) 
    for i in range(0,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xstar=(X[i-1]+2*S*dt)/(1+2*K*X[i-1]**P*dt)#X*
        Xnp1=(Xstar+X[i-1])/2
        Xe=(S/K)**(1/(P+1))#equilibrium solution
        X0=(S/K)**(1/(P+1))
        alpha=K*X0**P*dt
        Amplification[i]=(1-alpha*(P-1))/(1+2*alpha)#amplification factor
        X[i]=Xnp1
        t[i]=dt*i
        if abs(X[i]) > 1000:
            break
    return X,t,Amplification


# In[11]:


#Scheme.h but with the time filter applied only when the exchange coefficient is large
def SchemeHplus(X0,n,P,dt,K,A):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xe=(S/K)**(1/(P+1))#equilibrium solution
        Xnp1=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)#same as scheme d
        alp=K*Xe**P*dt
        if np.abs((1-alp*(P+1)/2)/(1+alp*(P+1)/2))>=1: #test if the the amplification factor is equal or larger than 1, applying time filter
            X[i]=(Xnp1)
        else:       #same as scheme.h
            Xstar=(X[i-1]+S*dt)/(1+K*X[i-1]**P*dt)#X*
            Xnp1=(1-A)*Xstar+A*X[i-1]
        X[i]=Xnp1
        t[i]=(dt*i)
        if abs(X[i]) > 1000:
            break

    return X,t


# In[12]:


#The scheme not mention in the section 2 but show on the thrid panel of the middle row.
def SchemeX(X0,n,P,dt,K):
    n=n+1
    X=np.zeros(n)
    t=np.zeros(n)
    Amplification=np.zeros(n)
    t[0]=0
    X[0]=(1/K)**(1/(P+1))
    X[1]=(SchemeHplus(X0,n,P,dt,K,0.5)[0][1])#for it is the three-time level scheme, X[1]will use other two-level scheme to calculate
    S=1+np.sin(2*np.pi*0/20*dt)
    Xe=(S/K)**(1/(P+1))#equilibrium solution

    for i in range(2,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        Xnp1=(X[i-1]+S*dt)/(1+K*(X[i-1]**P+X[i-2]**P)/2*dt)
        Xe=(S/K)**(1/(P+1))#equilibrium solution
        X[i]=(Xnp1)
        t[i]=(dt*i)
        if abs(X[i]) > 1000:
            break

    return X,t


# In[13]:


def f(X,t,P,S,K):#f is the function of local rate of change of ground temperature
    f=-(K*X**P)*X+S
    return f
def SchemeRK4(X0,n,P,dt,K): #RK4 method, X0 is the initial value of ground temperature
    n=n+1
    X=np.zeros(n)           #n is total number of time step
    t=np.zeros(n)           #P represent the nonlinear relation of exchange coefficient with temperature
    t[0]=0                  #dt, time step
    S=1+np.sin(2*np.pi*(0)/20*dt)
    X[0]=(S/K)**(1/(P+1))            
    TVD=0
    for i in range(1,n):
        S=1+np.sin(2*np.pi*(i)/20*dt)
        k1=f(X[i-1],i*dt,P,S,K)
        k2=f(X[i-1]+dt*k1/2,i*dt+dt/2,P,S,K)
        k3=f(X[i-1]+dt*k2/2,i*dt+dt/2,P,S,K)
        k4=f(X[i-1]+dt*k3,i*dt+dt,P,S,K)
        X[i]=X[i-1]+dt*(1/6)*(k1+2*k2+2*k3+k4)
        t[i]=dt*i
        TVD+=np.abs(X[i]-X[i-1])
    return X,t,TVD


# In[18]:




# In[ ]:




