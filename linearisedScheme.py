import numpy as np
import matplotlib.pyplot as plt

#Possible long time step scheme
def SchemePossible(n,P,dt,K):
    '''Describe all arguments and output in this doc-string'''
    n = n+1
    X = np.zeros(n)
    t = np.zeros(n)
    Xeq = np.zeros(n)
    t[0] = 0
    X[0] = (1/K)**(1/(P+1))
    Xeq[0] = X[0]
    
    for i in range(1,n):
        S = 1 + np.sin(2*np.pi*i/20*dt)
            
        Xeq[i] = (S/K)**(1/(P+1))

        #Initialise dX
        dX = 0.
        
        # Iterate to improve on dX
        for it in range(4):
            dX = (-K*dt*(Xeq[i] + dX)**(P+1)
                     + K*dt*Xeq[i]**P*dX*(P+1)
                     + S*dt - Xeq[i] + X[i-1])\
                    /(1 + K*dt*(P+1)*Xeq[i]**P)
        
        X[i] = Xeq[i] + dX
        t[i] = dt*i
    return X,t, Xeq


#Test of Possible long time step scheme
def main():
    dt = 0.5
    nSteps = 100
    K = 10
    P = 2
    B, A, C = SchemePossible(nSteps,P,dt,K)
    plt.plot(A,B,label='X')
    plt.plot(A,C,label='Xeq')
    plt.legend()
    print(B)
    plt.show()

main()

